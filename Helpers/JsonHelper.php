<?php

class JsonHelper {
	
	public static function send_response($slim_app, $array) {
		$slim_app->response()->header('Content-Type', 'application/json');
		echo json_encode($array);
	}
	
	public static function get_request($slim_app) {
		$request = $slim_app->request();
		$body = $request->getBody();
		return json_decode($body);
	}
	
}


?>