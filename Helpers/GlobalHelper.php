<?php

class GlobalHelper {
	private static $is_debug_mode;
	
	public static function is_debug_mode() {
		return self::$is_debug_mode;
	}
	
	public static function set_debug_mode($mode) {
		self::$is_debug_mode = $mode;
	}
	
}


?>