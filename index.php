<?php
/**
 * Step 1: Require the Slim PHP 5 Framework
 *
 * If using the default file layout, the `Slim/` directory
 * will already be on your include path. If you move the `Slim/`
 * directory elsewhere, ensure that it is added to your include path
 * or update this file path as needed.
 */

define('ROOT_PATH', dirname(__FILE__));

 
require 'Slim/Slim/Slim.php';
require 'NotORM/NotORM.php';
require_once 'Helpers/GlobalHelper.php';
require_once 'Helpers/JsonHelper.php';
require_once 'Services/MembershipService.php';
include_once 'Helpers/functions.php';


/**
 * Step 2: Instantiate the Slim application
 *
 * Here we instantiate the Slim application with its default settings.
 * However, we could also pass a key-value array of settings.
 * Refer to the online documentation for available settings.
 */
$app = new Slim(array(
	'MODE' => 'development',
	'TEMPLATES.PATH' => './templates'
));

$debug_mode = ($app->getMode() == 'development') ? true : false;
GlobalHelper::set_debug_mode($debug_mode);


/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, and `Slim::delete`
 * is an anonymous function. If you are using PHP < 5.3, the
 * second argument should be any variable that returns `true` for
 * `is_callable()`. An example GET route for PHP < 5.3 is:
 *
 * $app = new Slim();
 * $app->get('/hello/:name', 'myFunction');
 * function myFunction($name) { echo "Hello, $name"; }
 *
 * The routes below work with PHP >= 5.3.
 */

//GET route
$app->get('/', function () {
	echo 'Slim Application';
});

$app->post('/login', function() use($app) {
	$request = JsonHelper::get_request($app);
	$login_result = MembershipService::Login($request->mobile_number, $request->password);
	JsonHelper::send_response($app, $login_result);	
});

//PUT route
$app->put('/put', function () {
    echo 'This is a PUT route';
});

//DELETE route
$app->delete('/logout', function() use($app) {
    $request = JsonHelper::get_request($app);
	$logout_result = MembershipService::Logout($request->mobile_number);
	JsonHelper::send_response($app, $logout_result);
});

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This is responsible for executing
 * the Slim application using the settings and routes defined above.
 */
$app->run();
