<?php

require_once 'Result.php';
require_once ROOT_PATH . '/Models/UserModel.php';
require_once ROOT_PATH . '/Models/TokenModel.php';
include_once ROOT_PATH . '/Helpers/functions.php';

class MembershipService {
	
	public static function Login($mobile_number, $password) {
		$user = UserModel::get_user($mobile_number, $password);
		
		$login_result = new LoginResult();		
		if($user != null) {
			if($user['password'] == $password) { // necessary for case sensitivity of password
				$token = md5(uniqid($mobile_number, true));
				
				$result = TokenModel::insert_token($mobile_number, $token);
				if($result) {
					$login_result->success = true;
					$login_result->token = $token;					
				}
				else {
					$login_result->error_message = 'Can not create token';
				}
			}
		}
		else {
			$login_result->error_message = 'Authentication failed';
		}
		
		return $login_result;
	}
	
	public static function Logout($mobile_number) {
		$logout_result = new Result();
		
		$result = TokenModel::delete_token($mobile_number);
		if($result) {
			$logout_result->success = true;
		}
		else {
			$logout_result->error_message('Could not delete token');
		}
		return $logout_result;
	}
	
}

class LoginResult extends Result {
	public $token;
}

?>