<?php

require_once 'Model.php';

class ChunkModel extends Model {
	
	public static function get_count() {
		$db = parent::get_db();
		return $db->chunks()->count('*');
	}
	
	public static function get_chunks($limit, $offset) {
		$db = parent::get_db();
		$count = self::get_count();
		if($count < $offset) {
			return null;
		}
		else {
			return $db->chunks()
				->limit($limit, $offset);
		}
	}
	
}

?>