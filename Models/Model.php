<?php

require_once ROOT_PATH . '/NotORM/NotORM.php';
require_once ROOT_PATH . '/Helpers/GlobalHelper.php';

class Model {

	public static function get_db() {	
		$username = '';
		$password = '';	
		$db = '';
		
		if(GlobalHelper::is_debug_mode()) {	
			$username = 'root';
			$db_name = 'ad';
		}
		else {
			$user_name = '';
			$password = '';
			$db_name = '';
		}
		$dsn = "mysql:dbname=$db_name;host=localhost";
		
		$pdo = new PDO($dsn, $username, $password);
		return new NotORM($pdo);				
	}
	
}

?>