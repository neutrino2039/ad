<?php

require_once 'Model.php';

class TokenModel extends Model {
	
	public static function insert_token($mobile_number, $token) {		
		$db = parent::get_db();
		$row = $db->tokens()->insert(
			array(
				'mobile_number' => $mobile_number,
				'token' => $token
			)
		);
		
		if($row['mobile_number'] == null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static function delete_token($mobile_number) {
		$db = parent::get_db();
		$affected = $db->tokens()->delete('mobile_number', $mobile_number);
		if(affected > 0) {		
			return true;
		}
		else {
			return false;
		}
	} 		
}

?>