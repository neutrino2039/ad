<?php

require_once 'Model.php';

class UserModel extends Model {
	
	public static function get_user($mobile_number, $password) {		
		$db = parent::get_db();
		$user = $db->users()->select('mobile_number, password')
			->where(
				array(
					'mobile_number' => trim($mobile_number),
					'password' => $password
				)
			)->fetch();
		return $user;
	}
		
}

?>